Italic text represents commands  


1. **Requirements**   
    3GB absolute minimum storage (I recommend at least 10GB)    
    some form of Linux (MacOS and Windows may be supported using Vagrant to run Singularity)    
    

1. **Container Installation**    
    download either the tiago.sif or tiago-hybrid.sif container ﬁle  
    install sylabs singularity  
    clone tiago-lib from the sensible-robots gitlab group  
    move the downloaded .sif ﬁle into your cloned tiago-lib folder  
    

1. **Modules Installation**   
    *singularity build --sandbox tiago-rw tiago.sif*  
    *sudo singularity shell --writable tiago*  
    *. /code/python-env/bin/activate* (to activate the virtualenv)  
    *pip install face_recogntion*  
    *pip install google_cloud_speech*  
    if some modules were ommited, they can be installed by using the same procedure starting from the second command of this section  
    exit the container (ctrl-d/exit)  
    

1. **GitLab Files Location**   
    donwload the folders of the repository  
    place the folders in "tiago_lib/tiago_ws/src" directory  
    

1. **Execution**   
    Open 3 terminal windows in tiago-lib directory  
    *./run.sh* (in each terminal) (may take some time to build for the first time)  
    *catkin build* (for building the packages)  
    *source devel/setup.bash* (in each terminal)  
    *roslaunch tiago_2dnav_gazebo navigation.lauch* (in one terminal)  
    *roslaunch lasr_microphone microphone.launch* (in one terminal)  
    *rosrun lasr_microphone microphone_subscriber* (in one terminal)  

**The system is ready to use**

    