#!/usr/bin/env python

import threading
import rospy
from microphone_stream import MicrophoneStream
from std_msgs.msg import String
from dynamic_reconfigure.server import Server as rcfg_Server
from lasr_microphone.srv import ManageMicrophone, ManageMicrophoneResponse


class MicrophonePublisher(object):
    def __init__(self):
        self.enabled = None

        self.params = { 'auto_start': False }

        self.audio_stream = None
        self.audio_thread = None

        self.audio_pub = rospy.Publisher('microphone_audio', String, queue_size=10)

        self.mic_srv = rospy.Service('microphone_service', ManageMicrophone, self.manage_microphone_callback)

    def __del__(self):
        self.stop()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.stop()

    def manage_microphone_callback(self, req):
        print(req)
        changed = False
        if req.chunk_size != 0 and ('chunk_sz' not in self.params or self.params['chunk_sz'] != req.chunk_size):
            self.params['chunk_sz'] = req.chunk_size
            changed = True
        if req.rate != 0 and ('rate' not in self.params or self.params['rate'] != req.rate):
            self.params['rate'] = req.rate
            changed = True
        if req.channels != 0 and ('channels' not in self.params or self.params['channels'] != req.channels):
            self.params['channels'] = req.channels
            changed = True

        if changed and self.enabled:
            self.stop()

        if changed or self.audio_stream is None:
            self.audio_stream = MicrophoneStream(**self.params)

        if req.enabled:
            self.start()
        else:
            self.stop()

        return ManageMicrophoneResponse(success=True)

    def start(self):
        if not self.enabled or (self.enabled and not self.audio_thread):
            rospy.loginfo('starting audio publisher...')
            self.enabled = True
            self.audio_thread = threading.Thread(target=self._publish_audio)
            self.audio_thread.start()

    def stop(self):
        if self.enabled:
            rospy.loginfo('stopping audio publisher...')
            self.enabled = False
            if self.audio_stream:
                self.audio_stream.audio_buff.put(None)
            if self.audio_thread:
                self.audio_thread.join()
                self.audio_thread = None

    def _publish_audio(self):
        data = String()

        rospy.loginfo('publishing audio...')
        self.audio_stream.start()
        for audio_chunk in self.audio_stream.audio_generator():
            if not audio_chunk or not self.enabled:
                break
            data.data = audio_chunk
            self.audio_pub.publish(data)

        self.audio_stream.stop()
        rospy.loginfo('stopped publishing audio...')
