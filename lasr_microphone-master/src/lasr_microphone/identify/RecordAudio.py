#!/usr/bin/env python

import rospy
import sys
import os

from lasr_microphone import MicrophoneStream
import IdentifyFile,PrintAllProfiles,CreateProfile, EnrollProfile

#subscription on Microsoft Azure
subscription_key='250384615d764061979109439c1c19df'
locale='en-us'

#function to identify speaker from the API
def identify_speaker(filename):
    #first we must get all the profiles enrolled
    #calls print_all_profiles function from PrintAllProfiles file
    array=PrintAllProfiles.print_all_profiles(subscription_key)
    test_array=[]
    for elem in array:
        if (elem[1]=='Enrolled'):
            test_array.append(elem[0])

    #calls identify_file from IdentifyFile file
    #using the API checks in the voice recorded on voicetoberecognised.wav file exists
    speaker=IdentifyFile.identify_file(subscription_key,filename,"true",test_array )
    zero_check=speaker[0].replace("-","")

    if (zero_check=="00000000000000000000000000000000"):
        return "None found"
    return speaker

#function to create a new profile
#calls create_profile function from CreateProfile file
def create_new_profile():
    #it just creates the profile keeping an ID
    prof_id=CreateProfile.create_profile(subscription_key,locale)
    return prof_id

#function to enroll the recording on enroll.wav file using the ID of the newly created profile
def enroll_new_profile(prof_id,filename):
    EnrollProfile.enroll_profile(subscription_key,prof_id,filename, "true")

#function to print all profiles
def print_all():
    array=PrintAllProfiles.print_all_profiles(subscription_key)
    print(array)
