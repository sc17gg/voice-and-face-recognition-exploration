#!/usr/bin/env python

from ctypes import CFUNCTYPE, c_char_p, c_int, cdll
from six.moves import queue
import pyaudio
import time
import numpy as np


def get_current_time():
    return int(round(time.time()))


class MicrophoneStream(object):
    def __init__(
            self,
            frmt=pyaudio.paInt16,
            chunk_sz=1024,
            rate=16000,
            channels=1,
            streaming_limit=0.0,
            auto_start=True,
            get_channel_index=None
    ):
        error_handler_func = CFUNCTYPE(None, c_char_p, c_int, c_char_p, c_int, c_char_p)
        self.c_error_handler = error_handler_func(self.py_error_handler)
        self.asound = cdll.LoadLibrary('libasound.so')

        self.start_time = get_current_time()
        self.format = frmt
        self.chunk_sz = chunk_sz
        self.rate = rate
        self.streaming_limit = streaming_limit
        self.auto_start = auto_start
        self.closed = True

        self.channels = channels
        self.get_channel_index = get_channel_index
        if get_channel_index is not None:
            self.channels = 2

        self.audio_stream = None
        self.audio_buff = queue.Queue()

    def __del__(self):
        if self.audio_stream:
            self.__exit__(None, None, None)

    def __enter__(self):
        self.asound.snd_lib_error_set_handler(self.c_error_handler)
        self.audio_iface = pyaudio.PyAudio()
        self.audio_stream = self.audio_iface.open(
            rate=self.rate,
            format=self.format,
            channels=self.channels,
            frames_per_buffer=self.chunk_sz,
            input=True,
            start=self.auto_start,
            stream_callback=self.audio_stream_callback,
        )
        self.closed = not self.auto_start
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        self.stop()
        self.audio_stream.close()
        # Signal the generator to terminate
        self.audio_buff.put(None)
        self.audio_iface.terminate()
        self.asound.snd_lib_error_set_handler(None)
        self.audio_stream = None

    def start(self):
        if not self.audio_stream:
            self.__enter__()

        self.closed = False
        if self.audio_stream.is_stopped():
            self.audio_stream.start_stream()

    def stop(self):
        self.closed = True
        if not self.audio_stream.is_stopped():
            self.audio_stream.stop_stream()

    def py_error_handler(self, filename, line, function, err, fmt):
        pass

    def audio_stream_callback(self, in_data, frame_count, time_info, status_flags):
        self.audio_buff.put(in_data)
        return None, pyaudio.paContinue

    def audio_generator(self):
        while not self.closed:
            if self.streaming_limit and get_current_time() - self.start_time > self.streaming_limit:
                break

            # either make sure to get first chunk or we are finished
            chunk = self.audio_buff.get(block=True)
            if chunk is None:
                return
            data = [chunk] if self.get_channel_index is None else [
                self.get_data_by_channel(chunk, self.get_channel_index)]

            while True:
                try:
                    # check if finished, or get rest from queue
                    chunk = self.audio_buff.get(block=False)
                    if chunk is None:
                        return
                    chunk = chunk if self.get_channel_index is not None else self.get_data_by_channel(chunk,
                                                                                                      self.get_channel_index)
                    data.append(chunk)
                except queue.Empty:
                    break

            yield b''.join(data)

    def get_data_by_channel(self, data_in, channel_index=0):
        data_out = np.frombuffer(data_in, dtype=np.int16)
        data_out = np.stack((data_out[::2], data_out[1::2]), axis=0)
        data_out = data_out[channel_index, :]

        # data_out = np.fromstring(data_in, dtype=np.int16)
        # data_out = np.reshape(data_out, (self.chunk_sz, 2))
        # data_out = data_out[:, channel_index]

        return data_out.tobytes()

    @staticmethod
    def write_to_file(filename=None, output_path=None, time_secs=0.0, rate=16000, channels=1):
        """ Starts up the mic and records audio to {pkg}/output or output_path for time_secs"""
        import wave
        import rospkg
        from os import path, makedirs
        from datetime import datetime

        rospack = rospkg.RosPack()
        filename = filename or '{}.wav'.format('output')
        #filename = filename or '{}.wav'.format(datetime.now().strftime("%Y-%m-%d_%H:%M:%S"))
        if not output_path:
            filepath = path.join(rospack.get_path('lasr_microphone'), 'output', filename)
        else:
            filepath = path.join(output_path, filename)

        try:
            makedirs(path.dirname(filepath))
        except OSError:
            pass

        # with MicrophoneStream(rate=rate, channels=channels, streaming_limit=time_secs) as stream:
        with MicrophoneStream(rate=rate, channels=channels, streaming_limit=time_secs, get_channel_index=0) as stream:
            wav_file = wave.open(filepath, 'wb')
            wav_file.setnchannels(1)
            wav_file.setsampwidth(2)
            wav_file.setframerate(rate)

            try:
                for audio_chunk in stream.audio_generator():
                    if not audio_chunk:
                        break
                    wav_file.writeframes(audio_chunk)
            except KeyboardInterrupt:
                pass

        wav_file.close()

    @staticmethod
    def audio_intensity(time_secs):
        """ Gets average audio intensity of your mic sound. You can use it to get
            average intensities while you're talking and/or silent. The average
            is the avg of the 20% largest intensities recorded.
        """
        import audioop
        import math

        # TODO: allow setting params
        channels = 1
        rate = 16000
        chunk_sz = 1024

        print("Getting intensity values from mic.")
        values = []
        with MicrophoneStream(rate=rate, channels=channels, chunk_sz=chunk_sz, streaming_limit=time_secs) as stream:
            try:
                for audio_chunk in stream.audio_generator():
                    if not audio_chunk:
                        break
                    values.append(math.sqrt(abs(audioop.avg(audio_chunk, 2))))
            except KeyboardInterrupt:
                pass
        values = sorted(values, reverse=True)
        r = sum(values[:int(len(values) * 0.2)]) / int(len(values) * 0.2)
        print(" Finished ")
        print(" Average audio intensity is {}".format(r))
        return r

    @staticmethod
    def list_devices():
        """ Lists devices visible audio to PyAudio"""
        p = pyaudio.PyAudio()

        print('HOST API INFO')
        for i in range(p.get_host_api_count()):
            print(p.get_host_api_info_by_index(i))

        print('DEVICE INFO')
        for i in range(0, p.get_device_count()):
            print(p.get_device_info_by_index(i))


if __name__ == '__main__':
    MicrophoneStream.write_to_file(time_secs=10)
