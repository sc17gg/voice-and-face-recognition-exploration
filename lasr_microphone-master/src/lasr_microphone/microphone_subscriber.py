#!/usr/bin/env python

import rospy
import time
import wave
from std_msgs.msg import String
import dynamic_reconfigure.client
from six.moves import queue
from lasr_microphone.srv import ManageMicrophone, ManageMicrophoneRequest
from microphone_stream import MicrophoneStream

def get_current_time():
    return int(round(time.time()))

class MicrophoneSubscriber(object):
    def __init__(self, mic_topic, mic_service='microphone_service', auto_start_mic=True, rate=None, channels=None,
                 chunk_size=None):
        self.mic_service = mic_service

        self.config = ManageMicrophoneRequest()
        if rate:
            self.config.rate = rate
        if channels:
            self.config.channels = channels
        if chunk_size:
            self.config.chunk_size = chunk_size

        self.audio_buff = queue.Queue()
        self.microphone_streaming = False
        self.audio_streaming = False

        self.audio_sub = rospy.Subscriber(mic_topic, String, self.audio_callback)
        self.mic_srv = rospy.ServiceProxy(self.mic_service, ManageMicrophone)

        if auto_start_mic:
            self.start_microphone_streaming()

    def start_microphone_streaming(self):
        if not self.microphone_streaming:
            rospy.wait_for_service(self.mic_service, 15.0)
            try:
                self.config.enabled = True
                self.mic_srv(self.config)
                self.microphone_streaming = True
            except rospy.ServiceException, ex:
                rospy.logerr(ex)

    def stop_microphone_streaming(self):
        if self.microphone_streaming:
            rospy.wait_for_service(self.mic_service, 15.0)
            try:
                self.config.enabled = False
                self.mic_srv(self.config)
                self.microphone_streaming = False
            except rospy.ServiceException, ex:
                rospy.logerr(ex)

    def start_audio_streaming(self):
        if not self.audio_streaming:
            rospy.loginfo('starting audio subscription')
            self.start_time = get_current_time()
            # https://stackoverflow.com/questions/6517953/clear-all-items-from-the-queue
            self.audio_buff.mutex.acquire()
            self.audio_buff.queue.clear()
            self.audio_buff.all_tasks_done.notify_all()
            self.audio_buff.unfinished_tasks = 0
            self.audio_buff.mutex.release()

            self.audio_streaming = True

    def stop_audio_streaming(self):
        rospy.loginfo('stopping audio subscription')
        if self.audio_streaming:
            self.audio_streaming = False
            self.audio_buff.put(None)

    def audio_callback(self, data):
        if self.audio_streaming:
            self.audio_buff.put(data.data)

    def audio_generator(self,limit):
        #rospy.loginfo('starting to receive audio')
        while self.audio_streaming:
            if limit and get_current_time() - self.start_time > limit:
                break
            # either make sure to get first chunk or we are finished
            chunk = self.audio_buff.get(block=True)
            if chunk is None:
                return
            data = [chunk]

            while True:
                try:
                    # check if finished, or get rest from queue
                    chunk = self.audio_buff.get(block=False)
                    if chunk is None:
                        return
                    data.append(chunk)
                except queue.Empty:
                    break

            yield b''.join(data)

        #rospy.loginfo('stopping audio generator')
